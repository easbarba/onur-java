<!--
Onur is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Onur is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Onur. If not, see <https://www.gnu.org/licenses/>.
-->

## 0.4.0

- feat: new configuration with topics

## 0.3.0

- feat: add logging and bump updates

## 0.2.0

- feat: add database files discovery
- chore: improve ops
- feat: globals as info container
- feat: check if files are valid
- feat: parse all files
- feat: more tests
- feat: implement basic clone and pull actions
- refactor: improve solidness of config querying

## 0.1.0

- initial structure
